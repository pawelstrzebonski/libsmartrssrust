#[cfg(test)]
mod tests {
    #[test]
    fn empty_memory_backend() {
        // Setup test backend with no prior state
        let lsrr = crate::setup("").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_seen().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_unseen().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_unseen_limited(&10).unwrap().len(), 0);
    }
    #[test]
    fn save_empty_memory_backend() {
        // Setup test backend with no prior state
        let mut lsrr = crate::setup("").unwrap();
        // Save to file (should do nothing)
        lsrr.save_db()
    }
    #[test]
    fn empty_file_backend() {
        // Setup test backend with no prior state as a file
        let lsrr = crate::setup("test.bin").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_seen().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_unseen().unwrap().len(), 0);
        assert_eq!(lsrr.items_get_unseen_limited(&10).unwrap().len(), 0);
    }
    #[test]
    fn save_empty_file_backend() {
        // Setup test backend with no prior state as a file
        let mut lsrr = crate::setup("test.bin").unwrap();
        // Save to file
        lsrr.save_db()
    }
    #[test]
    fn add_feed_rss() {
        // Setup test backend with no prior state as a file
        let mut lsrr = crate::setup("test.bin").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        let feedurl = "http://www.rss-specifications.com/blog-feed.xml";
        lsrr.feed_add(feedurl, &3600);
        // We expect single feed now
        assert_eq!(lsrr.feeds_get().unwrap().len(), 1);
    }
    #[test]
    fn add_feed_atom() {
        // Setup test backend with no prior state as a file
        let mut lsrr = crate::setup("test.bin").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        let feedurl = "https://old.reddit.com/r/rust/.rss";
        lsrr.feed_add(feedurl, &3600);
        // We expect single feed now
        assert_eq!(lsrr.feeds_get().unwrap().len(), 1);
    }
    #[test]
    fn remove_feed() {
        // Setup test backend with no prior state as a file
        let mut lsrr = crate::setup("test.bin").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        let feedurl = "https://blog.rust-lang.org/feed.xml";
        lsrr.feed_add(feedurl, &3600);
        // We expect single feed now
        assert_eq!(lsrr.feeds_get().unwrap().len(), 1);
        // Remove added feed
        lsrr.feed_remove(feedurl);
        // We expect no feeds
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
    }
    #[test]
    fn rate_items() {
        // Setup test backend with no prior state as a file
        let mut lsrr = crate::setup("test.bin").unwrap();
        // We expect no existing data
        assert_eq!(lsrr.feeds_get().unwrap().len(), 0);
        let feedurl = "https://old.reddit.com/r/rust/.rss";
        lsrr.feed_add(feedurl, &3600);
        // We expect single feed now
        assert_eq!(lsrr.feeds_get().unwrap().len(), 1);
        let items = lsrr.items_get_unseen().unwrap();
        // We expect some items
        assert!(items.len() > 0);
        // Rate/open/view items
        for (i, item) in items.iter().enumerate() {
            if i % 2 == 0 {
                lsrr.item_like(&item.url);
            } else {
                lsrr.item_dislike(&item.url);
            }
            if i % 3 == 0 {
                lsrr.item_mark_opened(&item.url);
            }
            lsrr.item_mark_seen(&item.url);
        }
        // Add a second feed
        let feedurl = "https://blog.rust-lang.org/feed.xml";
        lsrr.feed_add(feedurl, &3600);
        // We expect two feeds now
        assert_eq!(lsrr.feeds_get().unwrap().len(), 2);
        let items = lsrr.items_get_unseen().unwrap();
        // We expect some items
        assert!(items.len() > 0);
        for i in 1..items.len() {
            // We expect items to be ordered by decreasing score
            assert!(items.get(i - 1).unwrap().score >= items.get(i).unwrap().score);
        }
    }
}
