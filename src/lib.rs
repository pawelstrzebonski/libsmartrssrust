// Disable dead code check as not all functions are called here
#![allow(dead_code)]

#[macro_use]
extern crate log;

mod classifier;
mod lib_tests;
use crate::classifier::MyClassifier;

use bincode::{config, Decode, Encode};
use std::collections::HashMap;
use std::io::{Read, Write};

/// Custom RSS feed struct
#[derive(Encode, Decode, Clone, Debug)]
pub struct MyFeed {
    pub url: String,       //URL of feed
    pub title: String,     //Title of feed
    pub lastupdated: i64,  //Datetime of last feed update
    pub updateperiod: i64, //Minimal time (in seconds) between feed updates
}

/// Custom RSS item struct
#[derive(Encode, Decode, Clone, Debug)]
pub struct MyItem {
    pub url: String,      //RSS item link
    pub title: String,    //RSS item title
    pub summary: String,  //RSS item summary
    pub tags: String,     //RSS item tags
    pub score: f64,       //Classifier's score of item
    pub rating: i64,      //User's rating of item
    pub opened: bool,     //Whether item has been opened by user
    pub seen: bool,       //Whether item has been shown to user
    pub lastupdated: i64, //Datetime of item addition
}

/// Item URL-Score pair
struct ItemScore {
    url: String, // Feed item URL
    score: f64,  // Feed item's classifier score
}

/// Export WordScore type
pub type WordScore = classifier::WordScore;

/// Back-end structure
pub struct LibSmartRSS {
    dbfeeds: HashMap<String, MyFeed>, // Look-up of RSS feeds by URL
    dbitems: HashMap<String, MyItem>, // Look-up of RSS feed items by URL
    itemslist: Vec<ItemScore>,        // (Sorted) list of unseen RSS feed item URLs
    class: MyClassifier,              // Text classifier
    filename: String,                 // Name of database save-file
    modified: bool,                   // Whether database has been modified since last save
}

fn filter_out_between(instr: &str, start: &char, stop: &char) -> String {
    let mut outstr = vec![];
    let mut buffer = vec![];
    let mut is_good = true;
    for c in instr.chars() {
        if !is_good {
            // If we are suspected within a bad block...
            if c == *stop {
                // If we hit a stop character, we exit it and clear the buffer
                buffer.clear();
                is_good = true;
            } else {
                // While still in possible bad block, append to buffer
                buffer.push(c);
            }
        } else {
            // If we are not in a bad block...
            if c == *start {
                // If we hit a start character, we add it to the buffer and mark as potential bad block
                buffer.push(c);
                is_good = false;
            } else {
                outstr.push(c);
            }
        }
    }
    outstr.extend(buffer);
    let outstr: String = outstr.iter().collect();
    outstr
}

/// Remove HTML tags from a string
fn string_sanitize(instr: &str) -> String {
    // Remove &.*?;
    let outstr = filter_out_between(instr, &'&', &';');
    // Remove <.*?>
    let outstr = filter_out_between(&outstr, &'<', &'>');
    // Remove tab and new line
    let outstr: String = outstr
        .chars()
        .filter(|c| *c != '\n' && *c != '\t')
        .collect();
    outstr
}

/// Given a URL, load, parse, and return the RSS channel structure
pub fn feed_get(url: &str) -> Option<(MyFeed, Vec<MyItem>)> {
    info!("Fetching feed from {}", url);
    // Some sites (e.g. reddit) will fail on request with no user agent. Use a common value?
    let response = minreq::get(url).with_header("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36").send().ok()?;
    let resp = response.as_str().ok()?;
    info!("Success: Fetched feed from {}", url);
    info!("Parsing feed from {}", url);
    let mut feeditems = vec![];
    // Parse into XML tree
    let doc = roxmltree::Document::parse(resp).ok()?;
    for node in doc.descendants() {
        if node.has_tag_name("entry") || node.has_tag_name("feed") {
            let mut url = "".to_string();
            let mut title = "".to_string();
            let mut summary = "".to_string();
            let mut tags = "".to_string();
            for child in node.children() {
                match child.tag_name().name() {
                    "link" => url = child.attribute("href").unwrap_or("").to_string(),
                    "title" => title = child.text().unwrap_or("").to_string(),
                    "content" | "description" => summary = child.text().unwrap_or("").to_string(),
                    "category" => {
                        tags.push(' ');
                        tags.push_str(
                            child
                                .attribute("label")
                                .unwrap_or_else(|| child.attribute("term").unwrap_or("")),
                        );
                        tags.push(' ');
                        tags.push_str(child.text().unwrap_or(""));
                    }
                    _ => (),
                }
            }
            feeditems.push(MyItem {
                url: url.to_string(),
                title: string_sanitize(&title).to_string(),
                summary: string_sanitize(&summary).to_string(),
                tags: string_sanitize(&tags).to_string(),
                opened: false,
                rating: 0,
                score: 0.0,
                seen: false,
                lastupdated: std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .as_secs() as i64,
            })
        }
    }
    let title = doc
        .descendants()
        .find_map(|n| {
            if n.has_tag_name("title") {
                Some(n.text().unwrap_or(""))
            } else {
                None
            }
        })
        .unwrap_or("");
    let feed = MyFeed {
        url: url.to_string(),
        title: string_sanitize(title),
        updateperiod: 0,
        lastupdated: std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64,
    };
    info!("Parsed feed from {}", url);
    Some((feed, feeditems))
}

impl LibSmartRSS {
    /// Add RSS feed to database
    pub fn feed_add(&mut self, url: &str, updateperiod: &i64) -> Option<()> {
        info!("Adding feed from {}", url);
        if !self.dbfeeds.contains_key(url) {
            let (mut new_feed, _) = feed_get(url)?;
            new_feed.updateperiod = *updateperiod;
            self.dbfeeds.insert(url.to_string(), new_feed);
            self.modified = true;
            info!("Feed {} added", url);
            self.feed_update(url);
            self.itemslist.sort_by(|a, b| {
                a.score
                    .partial_cmp(&b.score)
                    .unwrap_or(std::cmp::Ordering::Equal)
            });
        } else {
            info!("Feed {} already added", url);
        }
        Some(())
    }
    /// Update RSS feed with given URL
    fn feed_update(&mut self, url: &str) -> Option<()> {
        info!("Updating feed {}", url);
        let (_, feeditems) = feed_get(url)?;
        for item in feeditems {
            self.item_add(&item)?;
        }
        let n = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64;
        let mut feed = self.dbfeeds[url].clone();
        feed.lastupdated = n;
        self.dbfeeds.insert(url.to_string(), feed);
        self.modified = true;
        info!("Feed {} updated", url);
        Some(())
    }
    /// Add RSS item to database
    fn item_add(&mut self, new_item: &MyItem) -> Option<()> {
        let url = &new_item.url;
        info!("Adding item {}", url);
        if !self.dbitems.contains_key(url) {
            // Insert item into database
            self.dbitems.insert(url.to_string(), new_item.clone());
            self.modified = true;
            // And update its score as well
            let s = self.class.score_item(new_item);
            self.item_set_score(&new_item.url, &s);
            self.itemslist.push(ItemScore {
                url: url.clone(),
                score: s,
            });
            info!("Item {} added with score of {}", url, s);
        } else {
            info!("Item {} already added", url);
        }
        Some(())
    }
    /// Update all RSS feeds that are due for an update
    pub fn feeds_update(&mut self) -> Option<()> {
        info!("Updating feeds");
        let n = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64;
        let feeds_to_update: Vec<MyFeed> = self
            .dbfeeds
            .values()
            .filter(|&feed| feed.lastupdated + feed.updateperiod <= n)
            .cloned()
            .collect();
        for feed in &feeds_to_update {
            self.feed_update(&feed.url);
        }
        if !feeds_to_update.is_empty() {
            self.itemslist.sort_by(|a, b| {
                a.score
                    .partial_cmp(&b.score)
                    .unwrap_or(std::cmp::Ordering::Equal)
            });
        }
        info!("Updated feeds");
        Some(())
    }
    /// Mark RSS item with given URL as shown
    pub fn item_mark_seen(&mut self, url: &str) -> Option<()> {
        info!("Marking item {} seen", url);
        let mut item = self.dbitems[url].clone();
        item.seen = true;
        self.dbitems.insert(url.to_string(), item);
        // Update the classifier based on this item
        let item = &self.dbitems[url];
        self.modified = true;
        self.class.add_item(item);
        // Remove item from list of unread items
        let (idx, _) = self
            .itemslist
            .iter()
            .enumerate()
            .rev()
            .find(|(_, item)| item.url == url)
            .unwrap();
        self.itemslist.remove(idx);
        info!("Marked item {} seen", url);
        Some(())
    }
    /// Mark RSS item with given URL as opened
    pub fn item_mark_opened(&mut self, url: &str) -> Option<()> {
        info!("Marking item {} opened", url);
        let mut item = self.dbitems[url].clone();
        item.opened = true;
        self.dbitems.insert(url.to_string(), item);
        self.modified = true;
        info!("Marked item {} opened", url);
        Some(())
    }
    /// Mark RSS item with given URL as liked
    pub fn item_like(&mut self, url: &str) -> Option<()> {
        info!("Marking item {} liked", url);
        let mut item = self.dbitems[url].clone();
        item.rating = 1;
        self.dbitems.insert(url.to_string(), item);
        self.modified = true;
        info!("Marked item {} liked", url);
        Some(())
    }
    /// Mark RSS item with given URL as disliked
    pub fn item_dislike(&mut self, url: &str) -> Option<()> {
        info!("Marking item {} disliked", url);
        let mut item = self.dbitems[url].clone();
        item.rating = -1;
        self.dbitems.insert(url.to_string(), item);
        self.modified = true;
        info!("Marked item {} disliked", url);
        Some(())
    }
    /// Set score of RSS item with given URL
    pub fn item_set_score(&mut self, url: &str, score: &f64) -> Option<()> {
        info!("Setting score of item {}", url);
        let mut item = self.dbitems[url].clone();
        item.score = *score;
        self.dbitems.insert(url.to_string(), item);
        self.modified = true;
        info!("Set score of item {}", url);
        Some(())
    }
    /// Get seen RSS items
    fn items_get_seen(&self) -> Option<Vec<MyItem>> {
        let seen_items = self
            .dbitems
            .values()
            .filter(|&item| item.seen)
            .cloned()
            .collect();
        Some(seen_items)
    }
    /// Get unseen RSS items
    pub fn items_get_unseen(&self) -> Option<Vec<MyItem>> {
        let unseen_items: Vec<MyItem> = self
            .itemslist
            .iter()
            .rev()
            .map(|il| self.dbitems.get(&il.url).unwrap().clone())
            .collect();
        Some(unseen_items)
    }
    /// Get unseen RSS items
    pub fn items_get_unseen_limited(&self, limit: &i64) -> Option<Vec<MyItem>> {
        let unseen_items: Vec<MyItem> = self
            .itemslist
            .iter()
            .rev()
            .take(*limit as usize)
            .map(|il| self.dbitems.get(&il.url).unwrap().clone())
            .collect();
        Some(unseen_items)
    }
    /// Get all RSS feeds
    pub fn feeds_get(&self) -> Option<Vec<MyFeed>> {
        let all_feeds = self.dbfeeds.values().cloned().collect();
        Some(all_feeds)
    }
    /// Set score of RSS item with given URL
    pub fn feed_remove(&mut self, url: &str) -> Option<()> {
        info!("Removing feed {}", url);
        self.dbfeeds.remove(url);
        self.modified = true;
        info!("Removed feed {}", url);
        Some(())
    }
    /// Get all scored words
    pub fn word_scores_get(&mut self) -> Option<Vec<classifier::WordScore>> {
        // Get all seen items
        let items = self.items_get_seen()?;
        // Extract all words from them
        let mut words = vec![];
        for item in items {
            words.extend(classifier::item_to_input(&item));
        }
        // Sort and deduplicate (dedup depends on sorted vec)
        words.sort();
        words.dedup();
        // Score the words now
        Some(self.class.word_scores_get(&words))
    }
    /// Save database to file
    pub fn save_db(&mut self) {
        if self.modified && !self.filename.is_empty() {
            info!("Saving database to file");
            let config = config::standard();
            let ff = FileFormat {
                dbfeeds: self.dbfeeds.clone(),
                dbitems: self.dbitems.clone(),
            };
            let encoded: Vec<u8> = bincode::encode_to_vec(ff, config).unwrap();
            let mut f = std::fs::File::create(&self.filename).expect("Opening file");
            f.write_all(&encoded).expect("Writing to file");
            f.sync_data().expect("Syncing file data");
            self.modified = false;
        } else {
            info!("Not saving database to file");
        }
    }
}

/// Format of database save-file
#[derive(Encode, Decode)]
struct FileFormat {
    dbfeeds: HashMap<String, MyFeed>, // List of RSS feeds
    dbitems: HashMap<String, MyItem>, // List of RSS items
}

/// Create or load database at given file and setup the back-end, returning back-end object
pub fn setup(file: &str) -> Option<LibSmartRSS> {
    // Load data from save-file
    info!("Loading database...");
    let (dbfeeds, dbitems) = if !file.is_empty() && std::path::Path::new(file).exists() {
        let mut f = std::fs::File::open(file).expect("Opening file");
        let mut data = Vec::new();
        f.read_to_end(&mut data).expect("Reading file");
        let config = config::standard();
        let (decoded, _len): (FileFormat, usize) =
            bincode::decode_from_slice(&data, config).unwrap();
        (decoded.dbfeeds, decoded.dbitems)
    } else {
        (HashMap::new(), HashMap::new())
    };
    info!("Database loaded");
    // Create sorted list of scored item URL
    let mut itemslist: Vec<ItemScore> = dbitems
        .values()
        .filter(|&item| !item.seen)
        .cloned()
        .map(|item| ItemScore {
            url: item.url,
            score: item.score,
        })
        .collect();
    itemslist.sort_by(|a, b| {
        a.score
            .partial_cmp(&b.score)
            .unwrap_or(std::cmp::Ordering::Equal)
    });
    info!("Initiating classifiers...");
    let class = MyClassifier::new();
    let mut backend = LibSmartRSS {
        dbfeeds,
        dbitems,
        itemslist,
        class,
        filename: file.to_string(),
        modified: false,
    };
    info!("Classifiers initiated");
    info!("Training classifiers...");
    // Train classifier on all seen items
    for item in backend.items_get_seen()? {
        backend.class.add_item(&item);
    }
    info!("Classifiers trained");
    Some(backend)
}
