//! # Machine-learning (text classification) for the Smart-RSS application
//!
//! This module provides for the prediction of whether an RSS item will
//! be opened by the user and how the user would rate it.

use crate::MyItem;
use smol_str::SmolStr;
use std::collections::HashMap;

/// Word-Score pair
pub struct WordScore {
    pub word: SmolStr, // Word
    pub score: f64,    // Words's classifier score
}

/// Given an RSS item, return a vector of words contained in it's title,
/// summary, and tags.
pub fn item_to_input(item: &MyItem) -> Vec<SmolStr> {
    let min_word_length = 4;
    let mut input = vec![];
    for s in string_normalize(&item.title)
        .split(' ')
        .filter(|s| s.len() >= min_word_length)
    {
        input.push(SmolStr::from(s));
    }
    for s in string_normalize(&item.summary)
        .split(' ')
        .filter(|s| s.len() >= min_word_length)
    {
        input.push(SmolStr::from(s));
    }
    for s in string_normalize(&item.tags)
        .split(' ')
        .filter(|s| s.len() >= min_word_length)
    {
        input.push(SmolStr::from(s));
    }
    input
}

/// Remove/replace all non lowercase latin and space characters
pub fn string_normalize(in_string: &str) -> String {
    let out_string = in_string.chars().flat_map(char::to_lowercase);
    let out_string: String = out_string
        .filter(|x| x.is_alphanumeric() || *x == ' ')
        .collect();
    out_string
}

/// Count occurances of each word in list of string
fn count_words(doc: &Vec<SmolStr>) -> HashMap<SmolStr, u64> {
    let mut wc: HashMap<SmolStr, u64> = HashMap::new();
    for word in doc {
        let c = 1 + wc.get(word).unwrap_or(&0);
        wc.insert(word.clone(), c);
    }
    wc
}
/// Internal structure storing the text classifiers
pub struct MyClassifier {
    /// Total # of words encountered
    total_word_count: u64,
    /// Total # of documents encountered
    total_document_count: u64,
    /// Total # of documents encountered in a class
    documents_per_class: HashMap<SmolStr, u64>,
    /// Total # of documents of a certain class encountered containing a word
    documents_per_class_word: HashMap<(SmolStr, SmolStr), u64>,
    /// Total # of documents encountered containing a word
    documents_per_word: HashMap<SmolStr, u64>,
    /// Total # of word encounters
    word_count: HashMap<SmolStr, u64>,
    /// Total # of words encountered in class
    word_count_per_class: HashMap<SmolStr, u64>,
    /// Per-class/word count
    word_count_per_class_word: HashMap<(SmolStr, SmolStr), u64>,
}

impl MyClassifier {
    /// Create a new TF-IDF classifier
    pub fn new() -> MyClassifier {
        MyClassifier {
            total_word_count: 0,
            total_document_count: 0,
            documents_per_class: HashMap::new(),
            documents_per_class_word: HashMap::new(),
            documents_per_word: HashMap::new(),
            word_count: HashMap::new(),
            word_count_per_class: HashMap::new(),
            word_count_per_class_word: HashMap::new(),
        }
    }
    /// Add document (list of words) as belonging to list of classes
    fn add_document(&mut self, words: &Vec<SmolStr>, classes: &Vec<SmolStr>) {
        let wc = count_words(words);
        // Bump # of documents encountered
        self.total_document_count += 1;
        for class in classes {
            // Bump # of documents in this class encountered
            self.documents_per_class.insert(
                class.clone(),
                self.documents_per_class.get(class).unwrap_or(&0) + 1,
            );
            // Bump # of total words within this class
            self.word_count_per_class.insert(
                class.clone(),
                self.word_count_per_class.get(class).unwrap_or(&0) + (words.len() as u64),
            );
        }
        for (word, count) in &wc {
            // Bump # of documents with this word
            self.documents_per_word.insert(
                word.clone(),
                self.documents_per_word.get(word as &str).unwrap_or(&0) + 1,
            );
            // Bump total count of this word accross all documents
            self.word_count.insert(
                word.clone(),
                self.word_count.get(word).unwrap_or(&0) + count,
            );
            for class in classes {
                // Bump # of documents in this class seeing this word
                self.documents_per_class_word.insert(
                    (class.clone(), word.clone()),
                    self.documents_per_class_word
                        .get(&(class.clone(), word.clone()))
                        .unwrap_or(&0)
                        + 1,
                );
                // Bump word count within this particular class
                self.word_count_per_class_word.insert(
                    (class.clone(), word.clone()),
                    self.word_count_per_class_word
                        .get(&(class.clone(), word.clone()))
                        .unwrap_or(&0)
                        + 1,
                );
            }
        }
    }
    /// Calculate per-word TF-IDF scores given list of words
    fn tfidf(&self, words: &Vec<SmolStr>) -> HashMap<SmolStr, f64> {
        let wc = count_words(words);
        let mut scores = HashMap::new();
        for (word, count) in wc {
            // Term-frequency (proportion of words in document)
            let tf = (count as f64) / (words.len() as f64);
            // Inverse-document-frequncy (related to how many of all documents have this word)
            let idf = ((self.total_document_count as f64)
                / (1f64 + *self.documents_per_word.get(&word).unwrap_or(&0) as f64))
                .log(10f64);
            // Combine the two to get a TF-IDF score
            scores.insert(word, tf * idf);
        }
        scores
    }
    /// Calculate per-class Naive-Bayes estimation given list of words
    fn naivebayes(&self, words: &Vec<SmolStr>) -> HashMap<(SmolStr, SmolStr), f64> {
        let wc = count_words(words);
        let mut p_word: HashMap<SmolStr, f64> = HashMap::new();
        let mut p_class: HashMap<SmolStr, f64> = HashMap::new();
        let mut p_word_given_class: HashMap<(SmolStr, SmolStr), f64> = HashMap::new();
        let mut p_class_given_word: HashMap<(SmolStr, SmolStr), f64> = HashMap::new();
        for (class, count) in &self.documents_per_class {
            // Class probability based on fraction of all documents within this class
            //NOTE: Avoid dividing by zero by clamping denominator to >=1
            p_class.insert(
                class.clone(),
                (*count as f64) / (self.total_document_count as f64).max(1f64),
            );
        }
        for word in wc.keys() {
            // Word probability as fraction of all words
            //NOTE: Avoid dividing by zero by clamping denominator to >=1
            p_word.insert(
                word.clone(),
                (*self.word_count.get(word).unwrap_or(&0) as f64)
                    / (self.total_word_count as f64).max(1f64),
            );
            for class in self.documents_per_class.keys() {
                // Conditional probability of word given class (fraction of word within the class)
                //NOTE: Avoid dividing by zero by clamping denominator to >=1
                let key = (class.clone(), word.clone());
                let value = (*self.word_count_per_class_word.get(&key).unwrap_or(&0) as f64)
                    / (*self.word_count_per_class.get(class).unwrap_or(&1) as f64).max(1f64);
                p_word_given_class.insert(key, value);
            }
        }
        for (class, word) in p_word_given_class.keys() {
            // Bayesian estimation of probability of class given a particular word
            //NOTE: do some hacks to avoid issues from division by 0
            let mut value =
                p_class[class] * p_word_given_class[&(class.clone(), word.clone())] / p_word[word];
            if p_word[word] == 0f64 {
                value = 0f64;
            }
            p_class_given_word.insert((class.clone(), word.clone()), value);
        }
        p_class_given_word
    }
    /// Create a per-class score given list of words
    fn score(&self, words: &Vec<SmolStr>) -> HashMap<SmolStr, f64> {
        let mut scores = HashMap::new();
        // Calculate TF-IDF and Naive-Bayes estimations
        let t = self.tfidf(words);
        let n = self.naivebayes(words);
        for class in self.documents_per_class.keys() {
            // Iterate over each class
            let mut class_score = 0f64;
            for (word, word_score) in &t {
                // Get NB score for this class given this word
                let nb_score = *n.get(&(class.clone(), word.clone())).unwrap_or(&0f64);
                // Add log of NB score (instead of multiplying probabilities, add logs)
                // and weigh this by the TF-IDF score of this word
                if nb_score > 0f64 {
                    class_score += word_score * nb_score.log(10f64);
                }
            }
            // And save the total score for this category
            scores.insert(class.clone(), class_score);
        }
        scores
    }
    /// Calculate score for given item
    pub fn score_item(&self, item: &MyItem) -> f64 {
        // Get per-category scores of item
        let scores = self.score(&item_to_input(item));
        // Create overall score based on per-category scores
        return scores.get("opened").unwrap_or(&0f64) + scores.get("liked").unwrap_or(&0f64)
            - scores.get("disliked").unwrap_or(&0f64);
    }
    /// Add given item to classifier's memory
    pub fn add_item(&mut self, item: &MyItem) {
        let words = item_to_input(item);
        // Create a list of classes based on item
        let mut classes = Vec::new();
        if item.opened {
            classes.push(SmolStr::from("opened"));
        }
        match item.rating {
            1 => classes.push(SmolStr::from("liked")),
            -1 => classes.push(SmolStr::from("disliked")),
            0 => (),
            _ => unreachable!(),
        }
        self.add_document(&words, &classes);
    }
    /// Get list of scores for each of given words
    pub fn word_scores_get(&self, words: &Vec<SmolStr>) -> Vec<WordScore> {
        let mut ws = Vec::new();
        for word in words {
            let scores = self.score(&vec![word.clone()]);
            let s = scores.get("opened").unwrap_or(&0f64) + scores.get("liked").unwrap_or(&0f64)
                - scores.get("disliked").unwrap_or(&0f64);
            ws.push(WordScore {
                word: word.clone(),
                score: s,
            });
        }
        ws
    }
}
