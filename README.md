# libsmartrssrust

## About

Back-end library for machine learning enhanced RSS feed managers (written in Rust). It is used in a few front-end programs, such as the [native Rusty Smart-RSS app](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) and the [server/web UI Rusty Smart-RSS Web app](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web).

For more information about program design/implementation/use, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/libsmartrssrust/).

## Usage/Installation

As of writing this project is not stable and the API liable to change without notice. For usage, see the [native Rusty Smart-RSS app](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) and [Rusty Smart-RSS Web](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web)
