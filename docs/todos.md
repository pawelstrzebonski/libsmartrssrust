# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Automatic updates functionality? (currently handled at front-end level)
* Better handling of non-ASCII/international content
* Database cleanup:
	* remove old entries periodically
* General code/project improvements:
	* tests
	* CI
	* build documentation from source
