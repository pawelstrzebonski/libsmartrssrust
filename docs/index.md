# libsmartrssrust

libsmartrssrust is a back-end library for making machine learning enhanced RSS feed managers, written in Rust. It is a spin-off of my prior [Rusty Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) and [libsmartrssgo](https://gitlab.com/pawelstrzebonski/libsmartrssgo) projects and code, created with the intention of providing a common backend core that can be used for different frontend applications.

Existing front-ends include [Rusty Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) and [Rusty Smart-RSS Web](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web).
