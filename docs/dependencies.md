# Dependencies

This application makes use of the following Rust crates:

* [`minreq`](https://github.com/neonmoe/minreq): Fetch RSS feeds
* [`roxmltree`](https://github.com/RazrFalcon/roxmltree/): Parsing XML (for RSS/Atom feeds)
* [`log`](https://github.com/rust-lang/log): Logging of errors/events in the backend for debugging purposes
* [`regex`](https://github.com/rust-lang/regex): Regular expressions for text sanitization/normalization
* [`bincode`](https://github.com/bincode-org/bincode): Binary (de)serialization for saving/loading feed data to/from file
* [`lazy_static`](https://github.com/rust-lang-nursery/lazy-static.rs): Used to ensure regex rules are only compiled once
