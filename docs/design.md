# Design Document

## Files

This application has the following primary files:

* `src/lib.rs`
* `src/classifier.rs`

The `classifier.rs` file implements the text classifier and RSS item scoring code using a combination of naive Bayes and TF-IDF algorithma.

The `lib.rs` is the main document that implements the back-end object, `LibSmartRSS`, and the required methods. It uses the `classifier.rs`'s functions for machine learning, and defines within it the database setup and manipulation (with the "database" being a pair of hashmaps and a sorted list for ordered look-up of unread feed items, with a file save of data using `bincode`) and RSS/Atom feed fetching (`minreq`) and parsing (`roxmltree`).

The `lib_tests.rs` file contains some basic unit tests for this library.
