{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    nativeBuildInputs = [cargo pkg-config openssl];
}
